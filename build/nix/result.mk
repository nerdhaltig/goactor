.ONESHELL:

result-%:
	$(call in_nix_shell)
	
	$(eval OUT_LINK := $(shell echo "$@"))
	$(eval OUT_TARGET := $(shell echo "$@" | sed 's/result-//g'))
	echo -e "$(DEBUG) Build $(OUT_TARGET) to $(OUT_LINK) $(NORMAL)"
	nix-build -A $(OUT_TARGET) --out-link $(OUT_LINK)

clean: clean-result
clean-result:
	@rm -fv result-*

