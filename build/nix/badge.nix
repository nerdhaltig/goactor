{
  # core-stuff
  pkgs
  # packages
, anybadge ? pkgs.anybadge
  # options
, version ? "latest"
, badgename ? "badge"
, value
, suffix ? "%"
, red ? "50"
, orange ? "60"
, yellow ? "80"
, green ? "100"
}:
let
  #pythonEnv = pkgs.python3.withPackages ([ anybadge ]);
  pythonEnv = with pkgs.python311Packages; [ anybadge packaging ];
in
pkgs.stdenv.mkDerivation {
  pname = "badge-${badgename}";
  inherit version;
  src = null;
  unpackPhase = ''
    echo "unpacked"
  '';

  nativeBuildInputs = [
    pythonEnv
  ];

  outputs = [ "out" ];
  phases = [ "buildPhase" "installPhase" ];
  dontStrip = true;

  buildPhase = ''
    anybadge \
    --label=coverage \
    --value="${builtins.toString value}" \
    --suffix='${suffix}' \
    --overwrite \
    --file=badge.svg \
    ${red}=red ${orange}=orange ${yellow}=yellow ${green}=green
  '';

  installPhase = ''
    mkdir $out
    cp -v badge.svg $out/${badgename}.svg
  '';


}
