{
  # core-stuff
  pkgs
  # packages
, go ? pkgs.go
, gotestsum ? pkgs.gotestsum
  # options
, src
, package ? "."
, version ? "dev"
, debugging ? ""
}:
let
  ldflagsDebug =
    if debugging == "" then [
      "-s"
      "-w"
    ] else [ ];

in
pkgs.stdenv.mkDerivation {
  pname = "goactor-tests";
  inherit version;
  inherit src;

  nativeBuildInputs = [
    go
    gotestsum
  ];

  outputs = [ "out" ];

  dontStrip = true;

  buildPhase = ''
    export GOCACHE=$TMPDIR/go-cache
    packages=$(go list ${package}/... | grep -v /vendor/ )
    gotestsum --junitfile report.xml --format testname --packages $packages
  '';
  installPhase = ''
    mkdir $out
    cp -v report.xml $out/report.xml
  '';


}
