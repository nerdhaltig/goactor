
SYSTEM:=$(shell nix-instantiate --eval --expr 'builtins.currentSystem')
# aarch64

define in_nix_shell
	@if [ "${IN_NIX_SHELL}" == "" ]; then
		echo -e "$(INFO)running 'make $@' in nix-shell$(NORMAL)"
		nix-shell build/nix/shell.nix --command "make $@"
		exit 0
	else
		echo "We are in the nix-shell"
	fi
endef
