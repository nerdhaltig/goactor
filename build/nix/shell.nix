{}:
let
  nixpkgs = import ./nixpkgs.nix;
  pkgs = import nixpkgs { };

in
pkgs.mkShell {
  buildInputs = with pkgs; [
    stdenv
    shadow
    util-linux
    git
    git-secret
    gnupg
    curl

    zsh
    figlet

    pkg-config

    go
    gotestsum

    python39Packages.packaging
    python39Packages.anybadge
    graphviz
  ];
  shellHook = ''
    #!/bin/sh
    export NIX_PATH="nixpkgs=${nixpkgs}"
    export NIX_BUILD_CORES=4
    export IN_NIX_SHELL=true

    figlet "goactor - BuildSystem"

    export PATH=$PATH:${pkgs.python39Packages.anybadge}/bin
  '';
}

