{
  # core-stuff
  pkgs
  # packages
, go ? pkgs.go
  # options
, src
, package ? "."
, version ? "dev"
}:
let

in
pkgs.stdenv.mkDerivation {
  pname = "goactor-coverage";
  inherit version;
  inherit src;

  nativeBuildInputs = [
    go
  ];

  outputs = [ "out" ];

  dontStrip = true;

  buildPhase = ''
    export GOCACHE=$TMPDIR/go-cache
    PKG_LIST=$(go list ${package}/... | grep -v /vendor/ )
    go test -race -vet=all -coverprofile coverage.out -v ./... > coverage.log
    go tool cover -func coverage.out | grep total | tr -d "\t" | tr -d "total:(statements)" | tr -d "%" > coverage.sum

  '';
  installPhase = ''
    mkdir $out
    cp -v coverage.out $out/coverage.out
    cp -v coverage.sum $out/coverage.sum
  '';


}
