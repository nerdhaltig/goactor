
PROJECT_NAME := "goactor"
PKG := "gitlab.com/nerdhaltig/goactor/v2"
PKG_LIST := $(shell cd v2 && go list ${PKG}/... | grep -v /vendor/ )

update-modules: ## Update all go modules
	go get -u ./...
	go get -t -u ./...
	go mod tidy
	go mod vendor

test: ./coverage.out ## run tets

./coverage.out: in-nix-shell
	go test -race -coverprofile $@ -v $(PKG_LIST)

./.testCoverage.txt: ./coverage.out
	mv -v $< $@

./report.xml: in-nix-shell
	gotestsum --junitfile report.xml --format testname

memprofile-actor: memprofile-actor.out ## profile memory of actor package
memprofile-actor.out: in-nix-shell
	go test -memprofile $@ -v gitlab.com/nerdhaltig/goactor/actor
	go tool pprof -http localhost:9090 -no_browser -nodefraction=0 $@

memprofile-context: memprofile-context.out ## profile memory of actor package
memprofile-context.out: in-nix-shell
	go test -memprofile $@ -v gitlab.com/nerdhaltig/goactor/internal/jobcontext
	go tool pprof -http localhost:9090 -no_browser -nodefraction=0 $@

memprofile-supervisor: memprofile-supervisor.out ## profile memory of supervisor package
memprofile-supervisor.out: in-nix-shell
	go test -memprofile $@ -v gitlab.com/nerdhaltig/goactor/supervisor
	go tool pprof -http localhost:9090 -no_browser -nodefraction=0 $@

