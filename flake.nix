# nix --extra-experimental-features "nix-command flakes" build  .#coverage
# nix --extra-experimental-features "nix-command flakes" build  .#badge-coverage
{
  inputs = {
    # 15.05.2024
    nixpkgs.url = "https://github.com/NixOS/nixpkgs/archive/33d1e753c82ffc557b4a585c77de43d4c922ebb5.tar.gz";

    flake-utils.url = "github:numtide/flake-utils";
  };
  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs { inherit system; };

      in
      {
        packages = import ./default.nix { inherit pkgs; };

        # devShells.default = import ./shell2.nix { inherit pkgs; };

      });
}
