{ pkgs ? import (import build/nix/nixpkgs.nix) { }
}:
let
  lib = pkgs.lib;
  inherit (lib) sourceByRegex;

  version = "2.9.0";

  ################################### Binary ###################################
  src = pkgs.nix-gitignore.gitignoreSourcePure [ ] (pkgs.lib.cleanSource ./v2);

  coverage = import ./build/nix/coverage.nix { inherit pkgs src; };

  badge-test = import ./build/nix/badge.nix {
    inherit pkgs;
    value = import "${pkgs.writeText "$out/test" "10.0"}";
    badgename = "test";
  };

  badge-coverage = import ./build/nix/badge.nix {
    inherit pkgs;
    value = import "${coverage}/coverage.sum";
    badgename = "coverage";
  };

  badge = pkgs.symlinkJoin {
    name = "badges";
    paths = [ badge-coverage ];
  };

  report = import ./build/nix/report.nix { inherit pkgs src; };

in
{
  inherit
    coverage
    badge-test
    badge-coverage
    badge
    report;
}
