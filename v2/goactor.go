package goactor

import (
	"time"

	"gitlab.com/nerdhaltig/goactor/v2/actor"
	"gitlab.com/nerdhaltig/goactor/v2/async/adapter/watcherbased"
	"gitlab.com/nerdhaltig/goactor/v2/async/ports"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/logger"
	"gitlab.com/nerdhaltig/goactor/v2/workflow"
)

// NewActor create a new actor with an handling function.
//
// This will NOT spawn go-routines, call .Spawn() to do this.
type Actor actor.Actor

func NewActor(name string, handler func(ctx jobcontext.Ctx) (returnMessage []any, err error)) Actor {
	return actor.New(name, handler)
}

// ##### Context

// JobContext represents a job context
type JobContext jobcontext.Ctx

// NewJobContext create a single job-context
func NewJobContext(msg any, returnChannel chan []any) (ctx JobContext) {
	return jobcontext.New(msg, returnChannel)
}

// #####  Workflow

type WorkflowManager workflow.Manager

func NewWorkflowManager(opts ...workflow.ManagerOptions) WorkflowManager {
	return workflow.NewManager(opts...)
}

// ##### Concentrator

type Concentrator concentrator.Concentrator

func NewConcentrator(timeout time.Duration) Concentrator {
	return concentrator.New(timeout)
}

// ##### sync

type AsyncContext ports.AsyncContext

func NewAsyncContext(opts ...watcherbased.NewContextOpts) AsyncContext {
	return watcherbased.NewContext(opts...)
}

// ##### logger

func LoggerSet(log logger.Logger) {
	logger.SetLogger(log)
}

func DisableLogger() {
	logger.SetLogger(&logger.NoLogger{})
}
