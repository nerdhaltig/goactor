package logger

import "gitlab.com/nerdhaltig/goactor/v2/jobcontext"

type NoLogger struct{}

// OptsFromContext implements Logger.
func (*NoLogger) OptsFromContext(ctx jobcontext.Ctx) (opts *LoggerOpts) {
	return &LoggerOpts{}
}

// OptsToContext implements Logger.
func (*NoLogger) OptsToContext(ctx jobcontext.Ctx, opts *LoggerOpts) {}

// Debug implements Logger.
func (*NoLogger) Debug(opts *LoggerOpts, message string) {}

// Error implements Logger.
func (*NoLogger) Error(opts *LoggerOpts, message string) {}

// Info implements Logger.
func (*NoLogger) Info(opts *LoggerOpts, message string) {}

// Warning implements Logger.
func (*NoLogger) Warning(opts *LoggerOpts, message string) {}
