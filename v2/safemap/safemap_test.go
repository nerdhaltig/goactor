package safemap

import (
	"testing"

	"github.com/alecthomas/assert"
)

func TestMap(t *testing.T) {

	NewMap := New[string, int]()

	NewMap.Set("a", 1)
	NewMap.Set("b", 2)
	NewMap.Set("c", 3)

	counter := 0
	NewMap.ForEach(func(s string, i int) {
		counter++
	})
	assert.Equal(t, 3, counter)

	_, exist := NewMap.Get("d")
	assert.False(t, exist)
	counter = NewMap.Len()
	assert.Equal(t, 3, counter)

	value, _ := NewMap.Get("b")
	assert.Equal(t, 2, value)

	NewMap.Delete("b")
	_, exist = NewMap.Get("b")
	assert.False(t, exist)

	counter = NewMap.Len()
	assert.Equal(t, 2, counter)

	NewMap.DeleteFirst()
	counter = NewMap.Len()
	assert.Equal(t, 1, counter)
}
