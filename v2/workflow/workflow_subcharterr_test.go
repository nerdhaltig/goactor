package workflow_test

import (
	"testing"

	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/actor"
	"gitlab.com/nerdhaltig/goactor/v2/actorstates"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/workflow"
)

type firstMessageRequest struct{}
type firstMessageResponse struct{}

var firstMessageActor = actor.New("firstmsg", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
	switch ctx.Message().(type) {
	case
		*actorstates.Initialized,
		*actorstates.Started,
		*actorstates.Stopped:
		break
	case *firstMessageRequest:
		returnMessage = append(returnMessage, &firstMessageResponse{})
	}
	return
})

type secondMessageRequest struct{}
type secondMessageResponse struct{}

var secondMessageActor = actor.New("secondmsg", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
	switch ctx.Message().(type) {
	case
		*actorstates.Initialized,
		*actorstates.Started,
		*actorstates.Stopped:
		break
	case *secondMessageRequest:
		returnMessage = append(returnMessage, &secondMessageResponse{})
	}
	return
})

func TestSubchartError(t *testing.T) {

	var errors chan error = make(chan error, 1)

	manager := goactor.NewWorkflowManager(workflow.ManagerOptions{
		ErrorActor: actor.New("error", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
			switch msg := ctx.Message().(type) {
			case error:
				errors <- msg
			}
			return
		}),
	})

	// subflow that generate an error
	manager.New().RouteMessage([]*workflow.MessageToActor{
		{
			MessageType: &firstMessageResponse{},
			Actor:       errorGenerator,
		},
	})

	manager.New().
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &firstMessageRequest{},
				Actor:       firstMessageActor,
			},
		}).
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &firstMessageResponse{},
				Actor:       errorGenerator,
			},
		}).
		RouteMessageToNewWorkflows(&firstMessageResponse{})

	manager.Enqueue(&firstMessageRequest{})
	<-errors

}
