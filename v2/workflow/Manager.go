package workflow

import (
	"time"

	"gitlab.com/nerdhaltig/goactor/v2/actor"
)

type Manager interface {
	Enqueue(msg any)
	New() (newWorkflow *WorkflowTaskChain)
}

type ManagerData struct {
	Timeout time.Duration

	actor      actor.Actor
	errorActor actor.Actor
	Chains     map[string][]*WorkflowTaskChain
}

type WorkflowTaskChain struct {
	registered bool
	mgr        *ManagerData

	errorActor actor.Actor

	tasks []Task
}

type WorkflowID struct {
	ID string
}

func NewManager(optsArray ...ManagerOptions) (mgr Manager) {

	// ensure there is something in the array
	if len(optsArray) == 0 {
		optsArray = append(optsArray, ManagerOptions{})
	}

	// get opts
	opts := optsArray[0]

	// set defaults
	if opts.Timeout == 0 {
		opts.Timeout = time.Minute * 1
	}

	// new manager
	newManager := &ManagerData{
		Timeout:    opts.Timeout,
		errorActor: opts.ErrorActor,
		Chains:     make(map[string][]*WorkflowTaskChain),
	}

	// the actor
	newManager.actor = actor.New("workflow", newManager.handler)
	if opts.SpawnProcesses != 0 {
		newManager.actor.Spawn(opts.SpawnProcesses)
	}

	return newManager
}

func (m *ManagerData) New() (newWorkflow *WorkflowTaskChain) {

	// create new workflow
	newWorkflow = &WorkflowTaskChain{
		registered: false,
		mgr:        m,
		tasks:      []Task{},
	}

	return
}
