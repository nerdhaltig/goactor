package workflow_test

import (
	"errors"
	"math/big"
	"testing"

	"github.com/alecthomas/assert/v2"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/actor"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/workflow"
)

type loginRequest struct {
	Tenant   string
	Username string
	Password string
}
type loggedIn struct{}

type checkPasswordRequest struct {
	Tenant   string
	Username string
	Password string
}
type checkPasswordValid struct{}
type createTokenRequest struct {
	Tenant   string
	Username string
}
type createdToken struct {
	Token big.Int
}

// first actor
var loginActor = actor.New("login", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {

	switch msg := ctx.Message().(type) {
	case *loginRequest:

		return []any{
			&checkPasswordRequest{
				Tenant:   msg.Tenant,
				Username: msg.Username,
				Password: msg.Password,
			},
			&createTokenRequest{},
		}, err
	}

	return nil, err
})

// second actor
var passwordActor = actor.New("checkpassword", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {

	switch msg := ctx.Message().(type) {
	case *checkPasswordRequest:
		if msg.Username == "valid" {
			return []any{&checkPasswordValid{}}, nil
		}
	}

	err = errors.New("login incorrect")
	return nil, err
})

// third actor
var tokenActor = actor.New("token", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
	switch ctx.Message().(type) {
	case *createTokenRequest:

		// we produce some load
		r := new(big.Int)
		newToken := r.Binomial(1000, 10)

		return []any{&createdToken{
			Token: *newToken,
		}}, nil
	}

	return
})

// logging actor
var loggingActor = actor.New("logging", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {

	return nil, nil
})

// logging actor
var testNoError = actor.New("no error", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
	switch ctx.Message().(type) {
	case error:
		// check if there is a test-struct in our message-stack
		var currentTest *testing.T = &testing.T{}
		if ctx.As(&currentTest) {
			currentTest.FailNow()
		}
	}
	return nil, nil
})

func TestChain(t *testing.T) {

	// assert-actor
	// that check if all worked as expected

	expectActorDone := make(chan bool, 1)
	expectActor := actor.New("expect", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {

		switch ctx.Message().(type) {
		case *loggedIn:
			var exist bool
			_, exist = ctx.MessageGet("*workflow_test.loginRequest")
			assert.True(t, exist)
			_, exist = ctx.MessageGet("*workflow_test.checkPasswordRequest")
			assert.True(t, exist)
			_, exist = ctx.MessageGet("*workflow_test.createTokenRequest")
			assert.True(t, exist)
			_, exist = ctx.MessageGet("*workflow_test.checkPasswordValid")
			assert.True(t, exist)
			_, exist = ctx.MessageGet("*workflow_test.createdToken")
			assert.True(t, exist)
			_, exist = ctx.MessageGet("*workflow_test.loggedIn")
			assert.True(t, exist)

			expectActorDone <- true
		}
		return nil, nil
	})

	errActor := actor.New("expect", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		return nil, nil
	})

	// tag::example[]
	manager := goactor.NewWorkflowManager()

	// Basic informations:
	// when starting an workflow, every message enqueues and returned will be remember

	manager.New().

		// will call the errActor when an error occured inside an flow
		HandleError(errActor).

		// If an message will be enqueued to the manager and is of 'loginRequest',
		// it will be enqueued to 'loginActor'
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &loginRequest{},
				Actor:       loginActor,

				// Enqueued is set to true, because we would like to route
				// only enqueued message, not messages from prev contexts
				Direct: true,
			},
		}).

		// The loginActor return two messages ( *checkPasswordRequest and *createTokenRequest )
		// RouteReturnMessages will route return messages from prev actor to specific actors
		//
		// # Error handling
		//
		// - If an message from prev actor not exist, the workflow will continue.
		//
		// If you need to ensure that an prev actor return an message, use ExpectReturnMessageTypes()
		RouteMessage([]*workflow.MessageToActor{
			// message of `checkPasswordRequest` returned from the previouse actor will send to `passwordActor`
			{
				MessageType: &checkPasswordRequest{},
				Actor:       passwordActor,

				// Enqueued is set to false ( default ), because the prev actor
				// returned two messages and we not would like to handle the
				// original enqueued message
				Direct: false,
			},
			// message of `createTokenRequest` returned from the previouse actor will send to `tokenActor`
			{
				MessageType: &createTokenRequest{},
				Actor:       tokenActor,
			},
			// if you set no MessageType, this actor will not be called..
			{
				Actor: loggingActor,
			},
		}).

		// ExpectReturnMessageTypes only continue, if the expected messages-types are returned from
		// previouse actors. If an message of messageTypes is missing, an error occures and the
		// error actor will called if set.
		ExpectReturnMessageTypes(&checkPasswordValid{}, &createdToken{}).

		// If we collect the messages from the actors, we start
		// an new workflow, but keeping the current context.
		// So every next actor can read previouse messages
		//
		// but this will NOT be started in a new go-routine, so you can "wait"
		// for the sub-workflow to finish and do some more steps
		NewWorkflowWithMessage(&loggedIn{})

	// register second workflow, that only consist of
	// expectActor that handles *loginChecked
	manager.New().RouteMessage([]*workflow.MessageToActor{
		{
			MessageType: &loggedIn{},
			Actor:       expectActor,
		},
	})

	// we enqueue a message and start an workflow
	manager.Enqueue(&loginRequest{
		Tenant:   "default",
		Username: "valid",
		Password: "1234",
	})
	// end::example[]

	<-expectActorDone

}
