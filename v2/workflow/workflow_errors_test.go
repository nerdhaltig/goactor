package workflow_test

import (
	"testing"

	"github.com/alecthomas/assert/v2"
	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/actor"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/workflow"
)

// actor that generate an error
var errorGenerator = actor.New("errorGenerator", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
	return nil, errors.WithStack(errors.New("random error"))
})

func TestWorkflowError(t *testing.T) {

	// tag::example[]

	// an channel that hold our errors ( for testing )
	var errors chan error = make(chan error, 10)

	// this actor just handle errors and will write them to a channel for testing purpose
	errActor := actor.New("expectError", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {
		case error:
			errors <- msg
		}
		return nil, nil
	})

	manager := goactor.NewWorkflowManager()

	manager.New().
		// if an error occurred, call this actor but only for this workflow
		HandleError(errActor).
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &loginRequest{},
				Actor:       loginActor,
			},
		}).

		// we route checkPasswordRequest to an actor that just return an error
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &checkPasswordRequest{},
				Actor:       errorGenerator,
			},
		})

	// we enqueue a message and start an workflow
	manager.Enqueue(&loginRequest{
		Tenant:   "default",
		Username: "invalid",
		Password: "1234",
	})

	// wait for the error
	err := <-errors

	// end::example[]
	assert.Error(t, err)
	assert.EqualError(t, err, "random error")

}

func TestGlobalError(t *testing.T) {

	// tag::example[]

	// an channel that hold our errors ( for testing )
	var errors chan error = make(chan error, 10)

	// this actor just handle errors and will write them to a channel for testing purpose
	errActor := actor.New("expectError", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {
		case error:
			errors <- msg
		}
		return nil, nil
	})

	// tag::globalerroractor[]
	// you can define an global error-actor that call on every err on all workflows
	manager := goactor.NewWorkflowManager(workflow.ManagerOptions{
		ErrorActor: errActor,
	})
	// end::globalerroractor[]

	manager.New().
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &loginRequest{},
				Actor:       loginActor,
			},
		}).

		// we route checkPasswordRequest to an actor that just return an error
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &checkPasswordRequest{},
				Actor:       errorGenerator,
			},
		})

	// we enqueue a message and start an workflow
	manager.Enqueue(&loginRequest{
		Tenant:   "default",
		Username: "invalid",
		Password: "1234",
	})

	// wait for the error
	err := <-errors

	// end::example[]
	assert.Error(t, err)
	assert.EqualError(t, err, "random error")

}
