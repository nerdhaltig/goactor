package workflow_test

import (
	"testing"
	"time"

	"github.com/alecthomas/assert/v2"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/actor"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/workflow"
)

func TestTimeout(t *testing.T) {

	// tag::example[]

	// an channel that hold our errors ( for testing )
	var errors chan error = make(chan error, 10)

	// this actor just handle errors and will write them to a channel for testing purpose
	errActor := actor.New("expectError", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch msg := ctx.Message().(type) {
		case error:
			errors <- msg
		}
		return nil, nil
	})

	// a message that create a timeout
	type timeoutMessage struct{}

	// this actor just waits long
	timeoutActor := actor.New("timeoutActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {

		switch ctx.Message().(type) {
		case *timeoutMessage:
			time.Sleep(time.Second * 30)
		}

		return
	})
	timeoutActor.Spawn(10)

	manager := goactor.NewWorkflowManager(
		workflow.ManagerOptions{
			Timeout: time.Second * 1, // we setup the timeout
		},
	)

	// an workflow that timeouts and will definitly trigger the errActor
	manager.New().
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &timeoutMessage{},
				Actor:       timeoutActor,
			},
		}).

		// if an error occurred, call this actor
		HandleError(errActor)

	// this will trigger the timeout
	manager.Enqueue(&timeoutMessage{})

	err := <-errors
	// end::example[]

	assert.Error(t, err)
	assert.EqualError(t, err, "timeout waiting for actor to return")

}
