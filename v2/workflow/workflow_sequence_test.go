package workflow_test

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/actor"
	"gitlab.com/nerdhaltig/goactor/v2/actorstates"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/workflow"
)

func TestSequence(t *testing.T) {

	// tag::example[]

	// for example we try to implement an rest-request-workflow

	// RestRequest is an incoming rest request that
	// holds an RestMessage that should create a new workflow
	type RestRequest struct {
		RestMessage         any
		ExpectedMessageType any
		ExpectedMessage     chan any
	}

	// this actor use the RestMessage inside the RestRequest and return it
	// normally you would check tokens, and so on...
	var RestActor = actor.New("rest", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {

		switch msg := ctx.Message().(type) {

		// we ignore core messages
		case
			*actorstates.Initialized,
			*actorstates.Started,
			*actorstates.Stopped:
			break

		case *RestRequest:

			// check if we have already an answer
			msgReflect := reflect.ValueOf(msg.ExpectedMessageType)
			ReturnMessageTypeString := msgReflect.Type().String()

			payload, payloadExist := ctx.MessageGet(ReturnMessageTypeString)
			if payloadExist {
				msg.ExpectedMessage <- payload
			} else {
				returnMessage = append(returnMessage, msg.RestMessage)
			}

		}

		return
	})
	RestActor.Spawn(5)

	// this is an request that save an entity
	type EntitySave struct{}
	type EntitySaved struct{}

	// this actor just return that an entity was saved
	var EntitySaveActor = actor.New("saving", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {

		// open db.. save it.. check err and so on

		switch ctx.Message().(type) {

		// we ignore core messages
		case
			*actorstates.Initialized,
			*actorstates.Started,
			*actorstates.Stopped:
			break

		case *EntitySave:
			time.Sleep(time.Second * 5)
			returnMessage = append(returnMessage, &EntitySaved{})

		}

		return
	})
	EntitySaveActor.Spawn(5)

	manager := goactor.NewWorkflowManager(workflow.ManagerOptions{
		SpawnProcesses: 5,
	})

	// save workflow
	manager.New().
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &EntitySave{},
				Actor:       EntitySaveActor,
			},
		})

	// rest workflow
	manager.New().
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &RestRequest{},
				Actor:       RestActor,
			},
		}).
		RouteMessageToNewWorkflows(
			&EntitySave{}, // EntitySave will be returned by RestActor, we create a new workflow if this exist
		).
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &RestRequest{}, // we send it back when sub-workflows are finished
				Actor:       RestActor,
			},
		})

	newRequest := &RestRequest{
		RestMessage:         &EntitySave{},     // this we request
		ExpectedMessageType: &EntitySaved{},    // this we expect on our next channel
		ExpectedMessage:     make(chan any, 1), // the channel
	}
	manager.Enqueue(newRequest)

	<-newRequest.ExpectedMessage

	// end::example[]

}

func TestCorrectSequence(t *testing.T) {

	type FirstWorkflowStep1 struct{ finished chan interface{} }
	FirstWorkflowStep1Done := make(chan interface{}, 1)
	type FirstWorkflowStep2 struct{ finished chan interface{} }
	FirstWorkflowStep2Done := make(chan interface{}, 1)
	type SecondWorkflowStep1 struct{ finished chan interface{} }
	SecondWorkflowStep1Done := make(chan interface{}, 1)
	type SecondWorkflowStep2 struct{ finished chan interface{} }
	SecondWorkflowStep2Done := make(chan interface{}, 1)
	type SecondWorkflowStep3 struct{ finished chan interface{} }
	SecondWorkflowStep3Done := make(chan interface{}, 1)
	type FirstWorkflowStep3 struct{ finished chan interface{} }
	FirstWorkflowStep3Done := make(chan interface{}, 1)
	FirstWorkflowDone := make(chan interface{}, 1)

	stepActor := goactor.NewActor("stepping", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {

		switch msg := ctx.Message().(type) {
		case *FirstWorkflowStep1:
			time.Sleep(time.Second * 2)
			msg.finished <- nil
			returnMessage = append(returnMessage, &FirstWorkflowStep2{finished: FirstWorkflowStep2Done})

		case *FirstWorkflowStep2:
			time.Sleep(time.Second * 2)
			msg.finished <- nil
			returnMessage = append(returnMessage, &SecondWorkflowStep1{finished: SecondWorkflowStep1Done})

		case *SecondWorkflowStep1:
			time.Sleep(time.Second * 2)
			msg.finished <- nil
			returnMessage = append(returnMessage, &SecondWorkflowStep2{finished: SecondWorkflowStep2Done})

		case *SecondWorkflowStep2:
			time.Sleep(time.Second * 2)
			msg.finished <- nil
			returnMessage = append(returnMessage, &SecondWorkflowStep3{finished: SecondWorkflowStep3Done})

		case *SecondWorkflowStep3:
			time.Sleep(time.Second * 2)
			msg.finished <- nil
			returnMessage = append(returnMessage, &FirstWorkflowStep3{finished: FirstWorkflowStep3Done})

		case *FirstWorkflowStep3:
			time.Sleep(time.Second * 2)
			msg.finished <- nil

		}

		return
	})
	stepActor.Spawn(10)

	manager := goactor.NewWorkflowManager(workflow.ManagerOptions{
		SpawnProcesses: 10,
	})

	manager.New().
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &SecondWorkflowStep1{},
				Actor:       stepActor,
			},
		}).
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &SecondWorkflowStep2{},
				Actor:       stepActor,
			},
		}).
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &SecondWorkflowStep3{},
				Actor:       stepActor,
			},
		})

	manager.New().
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &FirstWorkflowStep1{},
				Actor:       stepActor,
			},
		}).
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &FirstWorkflowStep2{},
				Actor:       stepActor,
			},
		}).
		RouteMessageToNewWorkflows(
			&SecondWorkflowStep1{},
		).
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &FirstWorkflowStep3{},
				Actor:       stepActor,
			},
		}).
		Callback(func(ctx jobcontext.Ctx) {
			FirstWorkflowDone <- nil
		})

	manager.Enqueue(&FirstWorkflowStep1{finished: FirstWorkflowStep1Done})

	<-FirstWorkflowStep1Done
	<-FirstWorkflowStep2Done
	<-SecondWorkflowStep1Done
	<-SecondWorkflowStep2Done
	<-SecondWorkflowStep3Done
	<-FirstWorkflowStep3Done
	<-FirstWorkflowDone

}
