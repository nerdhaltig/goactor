package workflow_test

import (
	"testing"
	"time"

	"github.com/alecthomas/assert/v2"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/workflow"
)

func TestParallel(t *testing.T) {

	var results chan bool = make(chan bool, 10)

	// tag::example[]
	// In this example, we send one message to two actors in parallel
	type firstMessage struct {
		Data string
	}
	type result struct{}

	// our manager
	// that can enqueu 10 messages to workflows in parallel
	parallelWorkflow := goactor.NewWorkflowManager(workflow.ManagerOptions{
		SpawnProcesses: 2,
	})

	// our first actor that does just return firstResult
	firstActor := goactor.NewActor("first actor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		// tag::example[]
		switch ctx.Message().(type) {
		case *firstMessage:
			time.Sleep(time.Second * 3)
			results <- true
		}
		// end::example[]

		return []any{&result{}}, nil
	})
	parallelWorkflow.New().RouteMessage([]*workflow.MessageToActor{
		{
			MessageType: &firstMessage{},
			Actor:       firstActor,
		},
	})

	secondActor := goactor.NewActor("second actor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		// tag::example[]
		switch ctx.Message().(type) {
		case *firstMessage:
			time.Sleep(time.Second * 3)
			results <- true
		}
		// end::example[]

		return []any{&result{}}, nil
	})
	parallelWorkflow.New().RouteMessage([]*workflow.MessageToActor{
		{
			MessageType: &firstMessage{},
			Actor:       secondActor,
		},
	})

	// end::example[]

	// start an timer
	start := time.Now()

	// tag::example[]
	// and now we enqueue a message and trigger both workflows
	parallelWorkflow.Enqueue(&firstMessage{Data: "dummy"})

	// end::example[]

	// we wait for both messages
	<-results
	<-results
	elapsed := time.Since(start)
	assert.True(t, elapsed.Seconds() < 4, "Actors should take less than 4 seconds")

}
