package workflow_test

import (
	"testing"

	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/workflow"
)

func TestStress(t *testing.T) {

	var results chan uint32 = make(chan uint32, 1000)

	// our manager
	stressWorkflow := goactor.NewWorkflowManager(workflow.ManagerOptions{
		SpawnProcesses: 20,
	})

	// an actor that write numbers
	type stressMessage struct{}
	stressActor := goactor.NewActor("stresser", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {

		switch ctx.Message().(type) {
		case *stressMessage:
			results <- 1
		}

		return
	})
	stressActor.Spawn(10)

	stressWorkflow.New().
		RouteMessage([]*workflow.MessageToActor{
			{
				MessageType: &stressMessage{},
				Actor:       stressActor,
			},
		}).
		HandleError(testNoError)

	// send 300
	for loopIndex := 0; loopIndex < 300; loopIndex++ {
		stressWorkflow.Enqueue(&stressMessage{})
	}

	// read 300 back
	for loopIndex := 0; loopIndex < 300; loopIndex++ {
		<-results
	}
}
