module gitlab.com/nerdhaltig/goactor/v2

go 1.21

require (
	github.com/alecthomas/assert v1.0.0
	github.com/alecthomas/assert/v2 v2.9.0
	github.com/rs/zerolog v1.32.0
	github.com/teris-io/shortid v0.0.0-20220617161101-71ec9f2aa569
)

require github.com/matoous/go-nanoid/v2 v2.1.0

require (
	github.com/alecthomas/colour v0.1.0 // indirect
	github.com/alecthomas/repr v0.4.0 // indirect
	github.com/hexops/gotextdiff v1.0.3 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/pkg/errors v0.9.1
	github.com/sergi/go-diff v1.3.1 // indirect
	go.uber.org/goleak v1.3.0
	golang.org/x/sys v0.20.0 // indirect
)
