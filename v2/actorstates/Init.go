package actorstates

// Initialized occurred only once per actor and only for the first job handler.
//
// Use this if you want to init the actor state.
//
// You don't need to subscribe to this message, every actor will get it.
type Initialized struct{}

// Started occurred if an actor spawned.
//
// You don't need to subscribe to this message, every actor will get it.
type Started struct{}

// Stopped an actor.
//
// You don't need to subscribe to this message, every actor will get it.
type Stopped struct{}

// OrphanMessage occurred, when no actor is responsible for this message
//
// if you would like to handle this message, your actor need to subscribe to that.
type OrphanMessage struct {
	Msg any // the message
}

type ErrorMessage struct {
	Err error
}
