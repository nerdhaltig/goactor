package jobcontext

type CtxFull interface {
	Ctx
	CtxFork
}

type Ctx interface {

	// Message return the message for the current context
	Message() (msg any)

	// MessageType return the current type of message as string
	MessageType() string

	// MessageStore just store a message without setting it to the current active message
	MessageStore(msg any) (messageType string)

	// MessageGet return the current active message
	MessageGet(messageType string) (msg any, exist bool)

	// As will load the type of msgtype from the context.
	// Every Enqueue() and Return() append a message to the context chain
	// and this messages can loaded with As()
	//
	// Example:
	//
	//  var message *customMessage
	//  if ctx.As(&message) {
	As(msgtype any) (exist bool)

	// MessageExtract works like As() but delete the message from the context
	MessageExtract(msgtype any) (exist bool)

	// Error handling
	ErrSet(err error) Ctx
	Err() (err error)

	// ReturnChannel return the current return channel, can be nil !
	ReturnChannel() (returnChannel chan []any)

	// Return write an message and error to return channel if present
	// this will not store the message in the context !
	// msg or err can be nil
	Return(msg []any, err error)
}
