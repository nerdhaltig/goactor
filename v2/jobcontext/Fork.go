package jobcontext

import "reflect"

type CtxFork interface {
	// Fork will fork the context and return a new Ctx
	// You can for with a new Message ( which can be get with Message() )
	// or/and with a new return channel
	Fork(opts ForkOpts) Ctx
}

type ForkOpts struct {
	WithMessage any        // Use this as new message, otherwhise the original will be used
	WithChannel chan []any // use this as new return-channel
}

func (ctx *contextData) Fork(opts ForkOpts) Ctx {

	// new context
	newContext := &contextData{
		// msgs: ctx.msgs.ShallowClone(),
		msgs: ctx.msgs,
	}

	// new message
	if opts.WithMessage != nil {
		newContext.msg = opts.WithMessage

		msgReflect := reflect.ValueOf(newContext.msg)
		newContext.msgType = msgReflect.Type().String()

		newContext.msgs.Set(newContext.msgType, newContext.msg)
	} else {
		newContext.msg = ctx.msg
		newContext.msgType = ctx.msgType
	}

	// new context
	if opts.WithChannel != nil {
		newContext.returnChannel = opts.WithChannel
	} else {
		newContext.returnChannel = ctx.returnChannel
	}

	return newContext
}
