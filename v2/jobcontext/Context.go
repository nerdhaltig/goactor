package jobcontext

import (
	"reflect"

	"gitlab.com/nerdhaltig/goactor/v2/safemap"
)

// contextData represent the chain of messages
type contextData struct {
	msg     any
	msgType string

	// this hold all messages in a workflow
	//
	// this can be nil !
	msgs *safemap.SafeMap[string, any]

	// can be a message or err
	returnChannel chan []any
}

// New create a job-context
//
// returnChannel ( optional )
func New(msg any, returnChannel chan []any) (ctx CtxFull) {

	// new context
	newContext := &contextData{
		msg:           msg,
		msgType:       "nil",
		msgs:          safemap.New[string, any](),
		returnChannel: returnChannel,
	}

	// read message-type
	if msg != nil {
		msgReflect := reflect.ValueOf(msg)
		newContext.msgType = msgReflect.Type().String()

		newContext.msgs.Set(newContext.msgType, msg)
	}

	return newContext
}
