package jobcontext

type ctxError struct {
	err error
}

func (ctx *contextData) ErrSet(err error) Ctx {
	ctx.MessageStore(&ctxError{
		err: err,
	})
	return ctx
}

func (ctx *contextData) Err() (err error) {
	cError := &ctxError{}
	if ctx.As(&cError) {
		err = cError.err
	}
	return
}
