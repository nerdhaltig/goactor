package jobcontext_test

import (
	"testing"

	"github.com/alecthomas/assert/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func TestMessages(t *testing.T) {

	type firstMessage struct{ Data string }
	type secondMessage struct{ Data string }
	type thirdMessage struct{ Data string }

	fmsg := &firstMessage{
		Data: "first",
	}
	job := jobcontext.New(fmsg, nil)
	job.MessageStore(&secondMessage{
		Data: "second",
	})

	returnedMessage := job.Message()
	assert.Equal(t, fmsg, returnedMessage.(*firstMessage))

	curSecondMessage := &secondMessage{}
	job.As(&curSecondMessage)

	assert.Equal(t, "*jobcontext_test.firstMessage", job.MessageType())
	assert.Equal(t, "second", curSecondMessage.Data)

	curFirstMessage := &firstMessage{}
	curThirdMessage := &thirdMessage{}

	exist := job.As(&curFirstMessage)
	assert.Equal(t, true, exist)
	exist = job.As(&curSecondMessage)
	assert.Equal(t, true, exist)
	exist = job.As(&curThirdMessage)
	assert.Equal(t, false, exist)
}
