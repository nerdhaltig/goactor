package jobcontext_test

import (
	"testing"

	"github.com/alecthomas/assert/v2"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func TestForking(t *testing.T) {

	type firstMessage struct{ Data string }
	type secondMessage struct{ Data string }

	var firstMessages chan []any = make(chan []any, 10)
	defer close(firstMessages)

	firstCtx := jobcontext.New(firstMessage{Data: "first"}, firstMessages)
	secondCtx := firstCtx.(jobcontext.CtxFork).Fork(jobcontext.ForkOpts{
		WithMessage: secondMessage{Data: "second"},
	})

	// validate
	firstCtx.Return([]any{secondMessage{}}, nil)
	assert.Equal(t, 1, len(firstMessages))
	<-firstMessages

	secondCtx.Return([]any{secondMessage{}}, nil)
	assert.Equal(t, 1, len(firstMessages))
	<-firstMessages

	// first only has the first message
	msg, _ := firstCtx.MessageGet("jobcontext_test.firstMessage")
	assert.NotZero(t, msg)
	msg, _ = firstCtx.MessageGet("jobcontext_test.secondMessage")
	assert.NotZero(t, msg)

	msg, _ = secondCtx.MessageGet("jobcontext_test.secondMessage")
	assert.NotZero(t, msg)

}
