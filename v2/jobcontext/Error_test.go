package jobcontext_test

import (
	"testing"

	"github.com/alecthomas/assert/v2"
	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func TestError(t *testing.T) {

	// we create an empty context
	ctx := jobcontext.New(nil, nil)

	err := ctx.Err()
	assert.NoError(t, err)

	// set an error
	ctx.Return(nil, errors.WithStack(errors.New("test")))
	err = ctx.Err()
	assert.Error(t, err)
	assert.Equal(t, "test", err.Error())

}
