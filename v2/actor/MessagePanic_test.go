package actor_test

import (
	"testing"

	"github.com/alecthomas/assert/v2"
	"gitlab.com/nerdhaltig/goactor/v2/actor"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func TestPanic(t *testing.T) {
	// tag::example[]

	// this actor just panic
	panicActor := actor.New("panic", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		// we write to a nill thing: PAAAAAAAANIIIIICCC
		var isnil map[string]int
		isnil["test"] = 1

		return nil, nil
	})

	// channel that holds all return messages
	var returnChannel chan []any = make(chan []any, 10)

	// call the actor
	ctx := panicActor.Enqueue("test", returnChannel)

	// we have only one actor that return one array
	assert.Equal(t, 1, len(returnChannel))

	// read the array
	resultMessage := <-returnChannel

	// you can read the error
	err := ctx.Err()

	// end::example[]

	// it should be the error from panic
	assert.Equal(t, 1, len(resultMessage))
	assert.Error(t, err)

}
