package actor

func (act *actorData) InboxCounter() (usage, capacity int) {
	return len(act.inbox), cap(act.inbox)
}
