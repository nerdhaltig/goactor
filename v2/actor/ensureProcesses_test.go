package actor_test

import (
	"testing"
	"time"

	"github.com/alecthomas/assert"
	"gitlab.com/nerdhaltig/goactor/v2/actor"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func TestSpawn(t *testing.T) {
	// tag::example[]

	// this struct represent an request
	type requestMessage struct {
		Data string
	}

	// this struct represent an return message
	type resultMessage struct {
		Data string
	}

	// create a new actor with name "spawnActor"
	// the handler function will be called when an message is enqueued
	// per default there is no go-routine started
	testActor := actor.New("spawnActor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		return []any{
			&resultMessage{
				Data: "first",
			},
		}, nil
	})

	// end::example[]
	assert.Equal(t, 0, testActor.ProcessCounter())

	// tag::example[]

	// and this will spawn 10 more go-routines the actor will use
	testActor.Spawn(10)
	// end::example[]

	assert.Equal(t, 10, testActor.ProcessCounter())
	// tag::example[]

	// and you can spawn 5 more ( now 15 )
	testActor.Spawn(5)
	// end::example[]

	assert.Equal(t, 15, testActor.ProcessCounter())

	// tag::example[]

	// and you even can reduce them
	testActor.Spawn(-6)

	// we can use a channel that holds all return messages
	var returnChannel chan []any = make(chan []any, 1024)
	// end::example[]

	assert.Equal(t, 9, testActor.ProcessCounter())

	// send messages syncrhon
	for messageCounter := 100; messageCounter > 0; messageCounter-- {
		testActor.Enqueue("test", returnChannel)
	}

	// read all messages
	time.Sleep(time.Second * 2)
	channelMessageCounter := len(returnChannel)
	for channelMessageIndex := 0; channelMessageIndex < channelMessageCounter; channelMessageIndex++ {
		<-returnChannel
	}
	assert.Equal(t, 100, channelMessageCounter)

	// tag::example[]

	// create a struct
	request := &requestMessage{
		Data: "test",
	}

	// we now enqueue a struct, the actor use one of our spawned go-routines
	// call the handler and will write the result to returnChannel
	testActor.Enqueue(request, returnChannel)

	// and now you can read the result from the actor
	// it will be an []any
	var result []any = <-returnChannel
	// end::example[]

	_ = result

	for messageCounter := 99; messageCounter > 0; messageCounter-- {
		testActor.Enqueue("test", returnChannel)
	}

	// read all messages
	time.Sleep(time.Second * 2)
	channelMessageCounter = len(returnChannel)
	assert.Equal(t, 99, channelMessageCounter)
	for channelMessageIndex := 0; channelMessageIndex < channelMessageCounter; channelMessageIndex++ {
		<-returnChannel
	}

	testActor.Stop(time.Second * 10)
	assert.Equal(t, 0, testActor.ProcessCounter())

}
