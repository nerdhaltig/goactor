package actor

import (
	"errors"
	"fmt"
	"runtime/debug"

	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/logger"
)

func (act *actorData) messageHandler(ctx jobcontext.Ctx) {
	func() {

		// send error on panic
		defer func() {
			if recoverInterface := recover(); recoverInterface != nil {

				var newError error

				switch recoverData := recoverInterface.(type) {
				case error:
					//outErr = goactorerror.NewErrorWrapper(recoverData, ctx.Message())
					newError = recoverData
				case string:
					newError = errors.New(recoverData)
				//outErr = goactorerror.NewErrorWrapper(errors.New(recoverData), ctx.Message())
				default:
					newError = fmt.Errorf("%v", recoverData)
					//outErr = goactorerror.NewErrorWrapper(newError, ctx.Message())
				}

				logger.Log.Error(&logger.LoggerOpts{ActorName: act.name}, newError.Error())
				debug.PrintStack()

				//logger.Log.Error(errors.New(string(debug.Stack())))

				ctx.Return([]any{}, newError)
			}
		}()

		// call the handler
		logger.Log.Debug(&logger.LoggerOpts{ActorName: act.name}, fmt.Sprintf("call handler for message '%s'", ctx.MessageType()))
		returnMessages, err := act.handler(ctx)

		// ensure that we return something, even if its empty
		if returnMessages == nil {
			returnMessages = []any{}
		}

		// return
		ctx.Return(returnMessages, err)

	}()
}
