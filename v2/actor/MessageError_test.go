package actor_test

import (
	"testing"

	"github.com/pkg/errors"

	"github.com/alecthomas/assert/v2"
	"gitlab.com/nerdhaltig/goactor/v2/actor"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func TestError(t *testing.T) {
	// tag::example[]

	// this actor just return an error
	errorActor := actor.New("panic", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		return nil, errors.WithStack(errors.New("error"))
	})

	// channel that holds all return messages
	var returnChannel chan []any = make(chan []any, 10)

	// call the actor
	ctx := errorActor.Enqueue("test", returnChannel)

	// we have only one actor that return one array
	assert.Equal(t, 1, len(returnChannel))

	// read the array
	resultMessage := <-returnChannel

	// you can read the error
	err := ctx.Err()

	// end::example[]

	// result-channel contains error
	assert.Equal(t, 1, len(resultMessage))

	// but context contains an error
	assert.Error(t, err)

}
