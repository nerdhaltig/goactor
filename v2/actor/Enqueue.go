package actor

import (
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func (act *actorData) Enqueue(msg any, returnChannel chan []any) (ctx jobcontext.Ctx) {
	ctx = jobcontext.New(msg, returnChannel)
	act.EnqueueJob(ctx)
	return
}

func (act *actorData) EnqueueJob(ctx jobcontext.Ctx) {
	if act.inbox == nil {
		act.messageHandler(ctx)
	} else {
		act.inbox <- ctx
	}
}
