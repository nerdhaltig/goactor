package actor_test

import (
	"testing"

	"github.com/alecthomas/assert/v2"
	"gitlab.com/nerdhaltig/goactor/v2/actor"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

func TestMessageReturn(t *testing.T) {
	// tag::example[]

	type firstMessage struct {
		Data string
	}

	// a simple actor that basically does nothing
	// just return two messages
	returnMessageActor := actor.New("returnMessage", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		// this actor return two results
		return []any{
			&firstMessage{
				Data: "first",
			},
			"secondMessage",
		}, nil
	})

	// if we not use an return-channel, we can not read the return value
	// thats okay if your actor is an send-it-and-forget-actor
	returnMessageActor.Enqueue("test", nil)

	// better use an channel that holds all return messages
	var returnChannel chan []any = make(chan []any, 10)
	returnMessageActor.Enqueue("test", returnChannel)

	// one call, one result message, even if the actor return multiple messages
	// it return one array
	assert.Equal(t, 1, len(returnChannel))

	// here we read the returned message-array from returnChannel
	resultMessage := <-returnChannel

	// because its an array, it can have multiple messages
	assert.Equal(t, 2, len(resultMessage))
	assert.Equal(t, &firstMessage{
		Data: "first",
	}, resultMessage[0].(*firstMessage))
	assert.Equal(t, "secondMessage", resultMessage[1].(string))

	// end::example[]
}
