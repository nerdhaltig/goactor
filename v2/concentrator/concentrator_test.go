package concentrator_test

import (
	"errors"
	"testing"
	"time"

	"github.com/alecthomas/assert/v2"
	"gitlab.com/nerdhaltig/goactor/v2"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"go.uber.org/goleak"
)

type sleepRequst struct {
	SleepTime time.Duration
}
type sleeped struct {
	Done bool
}

var sleepActor = goactor.NewActor("sleepactor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
	switch msg := ctx.Message().(type) {
	case *sleepRequst:
		time.Sleep(msg.SleepTime)
		returnMessage = append(returnMessage, &sleeped{Done: true})
	}

	return
})

type sleepedHooked struct{ SleepTime time.Duration }

var sleepHookedActor = goactor.NewActor("sleepactor + 2seconds", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
	switch msg := ctx.Message().(type) {
	case *sleepRequst:
		time.Sleep(msg.SleepTime + 2*time.Second)
		returnMessage = append(returnMessage, &sleepedHooked{SleepTime: msg.SleepTime})
	}

	return
})

type echoRequest struct{ returnMessage any }

var echoActor = goactor.NewActor("echo", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
	switch msg := ctx.Message().(type) {
	case *echoRequest:
		time.Sleep(1 * time.Second)
		returnMessage = append(returnMessage, msg.returnMessage)
	}

	return
})

var counter int

type CountUpRequest struct{}

var countActor = goactor.NewActor("global counter", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
	switch ctx.Message().(type) {
	case *CountUpRequest:
		counter++
	}

	return
})

type DoErrorRequest struct{ err error }
type DoErrorRequested struct{}

var dropErrorActor = goactor.NewActor("drop error actor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
	switch msg := ctx.Message().(type) {
	case *DoErrorRequest:
		err = msg.err
		returnMessage = append(returnMessage, &DoErrorRequested{})
	}

	return
})

func init() {

}

func TestMessage(t *testing.T) {
	// prepare
	sleepActor.Spawn(2)
	sleepHookedActor.Spawn(2)
	defer goleak.VerifyNone(t)
	defer sleepActor.Stop(time.Minute * 1)
	defer sleepHookedActor.Stop(time.Minute * 1)

	ctr := concentrator.New(time.Second * 3)

	// get a message
	sleepResult := &sleeped{}

	_, err := ctr.WaitForMessages(nil,
		concentrator.Job{
			Actor:                 sleepActor,
			Message:               &sleepRequst{SleepTime: time.Second * 1},
			ExpectedReturnMessage: &sleepResult,
		},
	)
	assert.NoError(t, err)
	assert.True(t, sleepResult.Done)

	// test not returned message
	sleepedHooked := &sleepedHooked{}
	_, err = ctr.WaitForMessages(nil,
		concentrator.Job{
			Actor:                 sleepActor,
			Message:               &sleepRequst{SleepTime: time.Second * 1},
			ExpectedReturnMessage: &sleepedHooked,
		},
	)
	assert.Error(t, err)

}

func TestTimeout(t *testing.T) {
	// prepare
	sleepActor.Spawn(2)
	sleepHookedActor.Spawn(2)
	defer goleak.VerifyNone(t)
	defer sleepActor.Stop(time.Minute * 1)
	defer sleepHookedActor.Stop(time.Minute * 1)

	ctr := concentrator.New(time.Second * 3)

	// timeout a message
	curCtx, err := ctr.WaitForMessages(nil,
		concentrator.Job{
			Actor:   sleepActor,
			Message: &sleepRequst{SleepTime: time.Second * 4},
		},
	)
	assert.Error(t, err)
	result := &sleeped{}
	assert.False(t, curCtx.As(&result))

	// the same, job allowed to take longer
	curCtx, err = ctr.WaitForMessages(nil,
		concentrator.Job{
			Actor:   sleepActor,
			Message: &sleepRequst{SleepTime: time.Second * 4},
			Timeout: time.Second * 5,
		},
	)
	assert.NoError(t, err)
	assert.True(t, curCtx.As(&result))

	// test the same as hooked actor
	ctr.HookActor(sleepHookedActor, &sleepRequst{})

	hookedResult := &sleepedHooked{}
	curCtx, err = ctr.WaitForMessages(nil,
		concentrator.Job{
			Message: &sleepRequst{SleepTime: time.Second * 4},
			Timeout: time.Second * 15,
		},
	)
	assert.NoError(t, err)
	assert.True(t, curCtx.As(&hookedResult))

}

func TestParallel(t *testing.T) {
	// prepare
	sleepActor.Spawn(2)
	sleepHookedActor.Spawn(2)
	defer goleak.VerifyNone(t)
	defer sleepActor.Stop(time.Minute * 1)
	defer sleepHookedActor.Stop(time.Minute * 1)

	ctr := concentrator.New(time.Second * 5)

	// timeout a message
	curCtx, err := ctr.WaitForMessages(nil,
		concentrator.Job{
			Actor:   sleepActor,
			Message: &sleepRequst{SleepTime: time.Second * 1},
		},
		concentrator.Job{
			Actor:   sleepActor,
			Message: &sleepRequst{SleepTime: time.Second * 2},
		},
	)
	assert.NoError(t, err)
	firstResult := &sleeped{}
	assert.True(t, curCtx.As(&firstResult))
	secondResult := &sleeped{}
	assert.True(t, curCtx.As(&secondResult))
}

func TestChaining(t *testing.T) {
	// prepare
	sleepActor.Spawn(2)
	dropErrorActor.Spawn(2)
	defer goleak.VerifyNone(t)
	defer sleepActor.Stop(time.Minute * 1)
	defer dropErrorActor.Stop(time.Minute * 1)

	ctr := concentrator.New(time.Second * 5)

	type subactorRequest struct{}
	var subactor = goactor.NewActor("subactor", func(ctx jobcontext.Ctx) (returnMessage []any, err error) {
		switch ctx.Message().(type) {
		case *subactorRequest:

			// timeout a message
			_, err = ctr.WaitForMessages(ctx,
				concentrator.Job{
					Actor:   dropErrorActor,
					Message: &DoErrorRequest{err: errors.New("parallel error")},
				},
				concentrator.Job{
					Actor:   sleepActor,
					Message: &sleepRequst{SleepTime: time.Second * 1},
				},
			)
		}

		return
	})
	subactor.Spawn(2)
	defer subactor.Stop(time.Minute * 1)

	// timeout a message
	newCtx, err := ctr.WaitForMessages(nil,
		concentrator.Job{
			Actor:   subactor,
			Message: &subactorRequest{},
		},
	)
	assert.Error(t, err)

	// check if all other messages are there ?
	var isSleeped *sleeped = &sleeped{}
	assert.True(t, newCtx.As(&isSleeped))

}

func TestHooks(t *testing.T) {
	// prepare
	sleepActor.Spawn(2)
	sleepHookedActor.Spawn(2)
	countActor.Spawn(2)
	defer goleak.VerifyNone(t)
	defer sleepActor.Stop(time.Minute * 1)
	defer sleepHookedActor.Stop(time.Minute * 1)
	defer countActor.Stop(time.Minute * 1)

	ctr := concentrator.New(time.Second * 15)

	// add hooks
	ctr.HookActor(sleepHookedActor, &sleepRequst{})
	ctr.HookActor(countActor, &CountUpRequest{})

	// get a message
	resultWithActor := &sleepedHooked{}
	_, err := ctr.WaitForMessages(nil,
		concentrator.Job{
			Actor:                 sleepActor,
			Message:               &sleepRequst{SleepTime: time.Second * 1},
			ExpectedReturnMessage: &resultWithActor,
		},
	)
	assert.NoError(t, err)
	assert.Equal(t, time.Second*1, resultWithActor.SleepTime)

	// send a message without an actor
	resultWithoutActor := &sleepedHooked{}
	_, err = ctr.WaitForMessages(nil,
		concentrator.Job{
			Message:               &sleepRequst{SleepTime: time.Second * 1},
			ExpectedReturnMessage: &resultWithoutActor,
		},
	)
	assert.NoError(t, err)
	assert.Equal(t, time.Second*1, resultWithoutActor.SleepTime)

	// send, no hook is there but we expect an message
	errResp := &DoErrorRequested{}
	_, err = ctr.WaitForMessages(nil,
		concentrator.Job{
			Message:               &DoErrorRequest{},
			ExpectedReturnMessage: &errResp,
		},
	)
	assert.Error(t, err)

	// send, no hook is there, we not expect any message
	_, err = ctr.WaitForMessages(nil,
		concentrator.Job{
			Message: &DoErrorRequest{},
		},
	)
	assert.NoError(t, err)

	// we count up, should be 1
	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Message: &CountUpRequest{},
	})
	assert.NoError(t, err)
	assert.Equal(t, 1, counter)

	// with actor, it should not be sended twice
	_, err = ctr.WaitForMessages(nil, concentrator.Job{
		Actor:   countActor,
		Message: &CountUpRequest{},
	})
	assert.NoError(t, err)
	assert.Equal(t, 2, counter)

}

func TestErrors(t *testing.T) {
	// prepare
	dropErrorActor.Spawn(2)
	defer goleak.VerifyNone(t)
	defer dropErrorActor.Stop(time.Minute * 1)

	ctr := concentrator.New(time.Second * 15)

	// send an error
	_, err := ctr.WaitForMessages(nil, concentrator.Job{
		Actor:   dropErrorActor,
		Message: &DoErrorRequest{err: errors.New("an error")},
	})
	assert.Equal(t, err, errors.New("an error"))

}
