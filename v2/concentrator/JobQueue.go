package concentrator

import (
	"reflect"
	"sync"
	"time"

	gonanoid "github.com/matoous/go-nanoid/v2"
	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
)

// WaitForAllMessagesRequest will wait for all enqueued jobs to return messages
type WaitForAllMessagesRequest struct{}

// JobQueue represents an queue of jobs
type JobQueue struct {
	logit        zerolog.Logger
	concentrator *concentratorData
	ctx          jobcontext.Ctx

	returnChannel chan []any

	expectedReturnMessageCountSync sync.RWMutex
	expectedReturnMessageCount     int

	expectedReturnMessageDone chan bool
}

// Start creates a new jobqueue
func (c *concentratorData) Start() (newQueue *JobQueue) {

	newQueue = &JobQueue{}

	queueID, _ := gonanoid.New()
	newQueue.logit = log.With().
		Str("section", "jobqueue").
		Str("queueid", queueID).
		Logger()

	newQueue.concentrator = c

	newQueue.returnChannel = make(chan []any, 10)
	newQueue.expectedReturnMessageDone = nil

	newQueue.ctx = jobcontext.New(nil, newQueue.returnChannel)

	return
}

func (c *JobQueue) handleReturnMessages() {

	c.logit.Debug().Msg("Messageloop: Start reading in background")

	for {
		var returnedMessages []any

		// get the messages
		select {
		case msgs := <-c.returnChannel:
			returnedMessages = append(returnedMessages, msgs...)
		}

		// store it to context
		for _, msg := range returnedMessages {
			c.ctx.MessageStore(msg)
		}

		// decrement
		c.expectedReturnMessageCountSync.Lock()
		c.expectedReturnMessageCount--
		c.expectedReturnMessageCountSync.Unlock()

		// plaus check
		if c.expectedReturnMessageCount < 0 {
			c.logit.Error().Msg("more messages returned than expected")
		}

		// request to wait for all messages to finish
		if c.ctx.As(&WaitForAllMessagesRequest{}) {
			c.logit.Debug().Msg("Messageloop: Exit requested")
			if c.expectedReturnMessageCount != 0 {
				c.logit.Debug().Int("expected messages", c.expectedReturnMessageCount).Msg("Messageloop: We still wait for messages to return")
			} else {
				c.logit.Debug().Msg("Messageloop: Stop reading in background")
				c.ctx.MessageExtract(&WaitForAllMessagesRequest{})
				c.expectedReturnMessageDone <- true
				return
			}
		}

	}

}

// EnqueueMessage will enqueue a message async
func (c *JobQueue) EnqueueMessage(Message any) (err error) {

	c.logit.Debug().Msg("Enqueue new message")

	if c.expectedReturnMessageDone == nil {
		c.expectedReturnMessageDone = make(chan bool, 1)
		go c.handleReturnMessages()
	}

	go func() {

		// try to find an hooked actor
		msgReflect := reflect.ValueOf(Message)
		msgReflectString := msgReflect.Type().String()

		hookActors := c.concentrator.hookedActors[msgReflectString]

		// if we want an message, but it can not be fulfilled
		if len(hookActors) == 0 {
			err = errors.WithStack(errors.New("no task there to provide the expected message"))
			c.logit.Error().Err(err).Send()
		}

		// enqueue
		if err == nil {
			ctxWithFork := c.ctx.(jobcontext.CtxFork)

			// increment
			c.expectedReturnMessageCountSync.Lock()
			c.expectedReturnMessageCount = c.expectedReturnMessageCount + len(hookActors)
			c.expectedReturnMessageCountSync.Unlock()

			for _, actor := range hookActors {

				newJob := ctxWithFork.Fork(jobcontext.ForkOpts{
					WithMessage: Message,
				})

				actor.EnqueueJob(newJob)
			}
		}
	}()

	return
}

// WaitForFinished will wait for all enqueued messages to finish
//
// If timeout occurred, an error will returned
func (c *JobQueue) WaitForFinished(timeout time.Duration) (err error) {

	// no new message enqueued | read-messages-loop not enabled
	if c.expectedReturnMessageDone == nil {
		c.logit.Warn().Msg("no message enqueued, so nothing to wait for")
		return
	}

	waitTimer := time.NewTimer(timeout)

	// because we send a message, we need to increase the message counter
	c.expectedReturnMessageCountSync.Lock()
	c.expectedReturnMessageCount++
	c.expectedReturnMessageCountSync.Unlock()

	// we request an exit and wait for all messages
	c.returnChannel <- []any{WaitForAllMessagesRequest{}}

	select {
	case <-waitTimer.C:
		err = errors.WithStack(errors.New("timeout on waiting for finish"))
		c.logit.Error().Err(err).Send()
	case <-c.expectedReturnMessageDone:
		close(c.expectedReturnMessageDone)
		c.expectedReturnMessageDone = nil
		return
	}

	return
}

// As will load the type of message from the context.
//
// Example:
//
//	var message *customMessage
//	if ctx.As(&message) {
func (c *JobQueue) As(message any) (exist bool) {
	return c.ctx.As(message)
}
