// Package concentrator - a simple solution to chain and call actors
package concentrator

import (
	"fmt"
	"reflect"
	"time"

	gonanoid "github.com/matoous/go-nanoid/v2"
	"github.com/pkg/errors"
	"gitlab.com/nerdhaltig/goactor/v2/actor"
	"gitlab.com/nerdhaltig/goactor/v2/jobcontext"
	"gitlab.com/nerdhaltig/goactor/v2/logger"
)

// Concentrator represents the concentration of actors
type Concentrator interface {
	HookActor(actor actor.Actor, msg any)
	WaitForMessages(ctx jobcontext.Ctx, messagesToActor ...Job) (curCtx jobcontext.Ctx, err error)
	Start() (newQueue *JobQueue)
}

type concentratorData struct {
	timeout      time.Duration
	hookedActors map[string][]actor.Actor
}

// Job represent a single task to call an actor from an message
type Job struct {
	Actor                 actor.Actor // send 'Message' to 'Actor', if this is null, only hooks will take action if present
	Message               any         // message that will be sended to actor
	ExpectedReturnMessage any         // a pointer to an message that you expect, if the message not exist, an err will returned. Can be nil to not check an return message

	// timeout for this job-chain, if not set, the global timeout will be used
	// if you use multiple jobs, the max-timeout will be used
	// if your timeout is smaller than the global timeout, the global timeout is used
	Timeout time.Duration
}

// New create a new concentrator
func New(timeout time.Duration) Concentrator {
	return &concentratorData{
		timeout:      timeout,
		hookedActors: make(map[string][]actor.Actor),
	}
}

// WaitForMessages will wait for all actors to return and update the ctx
// if ctx is nil, a new one will be created
func (c *concentratorData) WaitForMessages(ctx jobcontext.Ctx, jobs ...Job) (curCtx jobcontext.Ctx, err error) {

	// logger
	loggerOpts := &logger.LoggerOpts{}
	loggerOpts.WorkflowID, _ = gonanoid.New()

	// we need an return-channel to wait for message
	var returnChannel chan []any
	var returnChannelCreated bool

	// create context/returnChannel if needed
	if ctx == nil {
		logger.Log.Debug(loggerOpts, "returnChannel: create our own message-channel")

		returnChannel = make(chan []any, 10)
		returnChannelCreated = true
		ctx = jobcontext.New(nil, returnChannel)
	} else {
		ctx = ctx.(jobcontext.CtxFork).Fork(jobcontext.ForkOpts{
			WithChannel: make(chan []any, 10),
		})
		returnChannel = ctx.ReturnChannel()
	}

	// set the return-context
	curCtx = ctx

	logger.Log.Debug(loggerOpts, "Collecting tasks...")

	// add hooks
	var jobsFromHooks []Job
	for _, job := range jobs {
		msgReflect := reflect.ValueOf(job.Message)
		msgReflectString := msgReflect.Type().String()

		hookActors := c.hookedActors[msgReflectString]

		// if we want an message, but it can not be fulfilled
		if job.Actor == nil && job.ExpectedReturnMessage != nil && len(hookActors) == 0 {
			err = errors.WithStack(errors.New("no task there to provide the expected message"))
			return
		}

		for _, hookActor := range hookActors {

			// should not be the same actor !
			if hookActor == job.Actor {
				continue
			}

			jobsFromHooks = append(jobsFromHooks, Job{
				Actor:                 hookActor,
				Message:               job.Message,
				ExpectedReturnMessage: job.ExpectedReturnMessage,
				Timeout:               job.Timeout,
			})
		}
	}

	// filter messagesToActor
	var jobsFromParameter []Job
	for _, messageToActor := range jobs {
		if messageToActor.Actor != nil {
			jobsFromParameter = append(jobsFromParameter, messageToActor)
		}
	}

	// build new array
	jobs = []Job{}
	jobs = append(jobs, jobsFromParameter...)
	jobs = append(jobs, jobsFromHooks...)

	// routine that write and count return messagesEnqueued
	messagesEnqueued := len(jobs)
	messagesSaved := 0
	messagesDone := make(chan any, 1)

	logger.Log.Debug(loggerOpts, fmt.Sprintf("Collecting tasks...done. Found: %d", messagesEnqueued))

	// nothing to do
	if messagesEnqueued == 0 {
		return
	}

	logger.Log.Debug(loggerOpts, "Messageloop: Start reading in background")
	var returnedMessages []any
	go func(returnChannel chan []any, returnChannelCreated bool) {
		for {

			msgs := <-returnChannel
			returnedMessages = append(returnedMessages, msgs...)

			messagesSaved++
			if messagesSaved == messagesEnqueued {
				logger.Log.Debug(loggerOpts, "Messageloop: All actors responded")

				messagesDone <- nil
				close(messagesDone)

				if returnChannelCreated {
					logger.Log.Debug(loggerOpts, "returnChannel: close channel")
					close(returnChannel)
				}
				return
			}

		}
	}(returnChannel, returnChannelCreated)

	// enqueu
	ctxWithFork := ctx.(jobcontext.CtxFork)

	logger.Log.Debug(loggerOpts, "Actors: Start to call all actors")

	waitForMessageTimeout := time.NewTimer(c.timeout)
	for _, job := range jobs {

		if job.Timeout > c.timeout {
			waitForMessageTimeout = time.NewTimer(job.Timeout)
		}

		newJob := ctxWithFork.Fork(jobcontext.ForkOpts{
			WithMessage: job.Message,
			WithChannel: returnChannel,
		})

		job.Actor.EnqueueJob(newJob)
	}

	logger.Log.Debug(loggerOpts, "Actors: Done")

	// wait
	select {
	case <-messagesDone:
		break
	case <-waitForMessageTimeout.C:
		err = errors.New("timeout on waiting for messages")
		logger.Log.Error(loggerOpts, err.Error())
		ctx.ErrSet(err)
	}

	logger.Log.Debug(loggerOpts, fmt.Sprintf("Actors: Returned messages: %d", len(returnedMessages)))

	// store it to context
	for _, msg := range returnedMessages {
		ctx.MessageStore(msg)
	}

	// read error
	err = ctx.Err()

	// check expected return messages
	if err == nil {
		for _, job := range jobs {
			if job.ExpectedReturnMessage != nil {
				if !ctx.As(job.ExpectedReturnMessage) {
					err = errors.WithStack(fmt.Errorf("expected return message missing in context: %s", reflect.ValueOf(job.ExpectedReturnMessage).Elem().Type().String()))
					break
				}
			}
		}
	}

	// enqueue return messages to hooks
	var jobsFromReturnMessages []Job
	if err == nil {
		for _, msg := range returnedMessages {
			jobsFromReturnMessages = append(jobsFromReturnMessages, Job{
				Message: msg,
			})
		}
		if len(jobsFromReturnMessages) > 0 {
			curCtx, err = c.WaitForMessages(ctx, jobsFromReturnMessages...)
		}
	}

	return
}

func (c *concentratorData) HookActor(actor actor.Actor, msg any) {
	msgReflect := reflect.ValueOf(msg)
	msgReflectString := msgReflect.Type().String()

	currentActors := c.hookedActors[msgReflectString]
	currentActors = append(currentActors, actor)
	c.hookedActors[msgReflectString] = currentActors
}
