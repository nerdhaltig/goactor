package concentrator_test

import (
	"testing"
	"time"

	"github.com/alecthomas/assert/v2"
	"gitlab.com/nerdhaltig/goactor/v2/concentrator"
	"go.uber.org/goleak"
)

func TestJobQueue(t *testing.T) {

	// prepare
	defer goleak.VerifyNone(t)
	sleepHookedActor.Spawn(2)
	defer sleepHookedActor.Stop(time.Minute * 1)

	ctr := concentrator.New(time.Second * 3)
	ctr.HookActor(sleepHookedActor, &sleepRequst{})

	// create a queue
	queue := ctr.Start()

	// call some
	err := queue.EnqueueMessage(&sleepRequst{})
	assert.NoError(t, err)

	// wait for finished
	err = queue.WaitForFinished(time.Second * 5)
	assert.NoError(t, err)

	// already finished, no error
	err = queue.WaitForFinished(time.Second * 5)
	assert.NoError(t, err)

}

type JobMessage1 struct{}
type JobMessage2 struct{}
type JobMessage3 struct{}
type JobMessage4 struct{}
type JobMessage5 struct{}

func TestMultipleMessageJobQueue(t *testing.T) {
	// prepare
	defer goleak.VerifyNone(t)

	// we need an echo-actor
	echoActor.Spawn(2)
	defer echoActor.Stop(time.Minute * 1)

	ctr := concentrator.New(time.Second * 3)
	ctr.HookActor(echoActor, &echoRequest{})

	// create a queue
	queue := ctr.Start()

	// multiple messages
	queue.EnqueueMessage(&echoRequest{returnMessage: &JobMessage1{}})
	queue.EnqueueMessage(&echoRequest{returnMessage: &JobMessage2{}})
	queue.EnqueueMessage(&echoRequest{returnMessage: &JobMessage3{}})
	queue.EnqueueMessage(&echoRequest{returnMessage: &JobMessage4{}})
	queue.EnqueueMessage(&echoRequest{returnMessage: &JobMessage5{}})

	err := queue.WaitForFinished(time.Second * 5)
	assert.NoError(t, err)

	// check that we get all messages
	retJobMessage1 := &JobMessage1{}
	assert.True(t, queue.As(&retJobMessage1))

	retJobMessage2 := &JobMessage1{}
	assert.True(t, queue.As(&retJobMessage2))

	retJobMessage3 := &JobMessage1{}
	assert.True(t, queue.As(&retJobMessage3))

	retJobMessage4 := &JobMessage1{}
	assert.True(t, queue.As(&retJobMessage4))

	retJobMessage5 := &JobMessage1{}
	assert.True(t, queue.As(&retJobMessage5))
}
