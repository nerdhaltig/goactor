package error

import "fmt"

type ErrorWithMessage interface {
	Message() any
	Error() string
	Unwrap() error
}

type WithMessage struct {
	Msg any   `json:"message"` // Human readable message for clients
	err error `json:"-"`       // The original error. Same reason as above.
}

func (err WithMessage) Message() any {
	return err.Msg
}

// Returns Message if Err is nil
func (err WithMessage) Error() string {
	if err.err == nil {
		if err.Msg != nil {
			return fmt.Sprintf("%p", &err.Msg)
		}
	} else {
		return err.err.Error()
	}
	return ""
}

func (err WithMessage) Unwrap() error {
	return err.err // Returns inner error
}

func NewErrorWrapper(err error, message any) error {
	return &WithMessage{
		Msg: message,
		err: err,
	}
}
