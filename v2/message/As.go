package message

import (
	"reflect"
	"strings"
)

func (lmsg *mapBased) As(msgtype any) (exist bool) {

	msgReflect := reflect.ValueOf(msgtype)
	msgRelflectType := msgReflect.Type().String()
	msgRelflectType = strings.Trim(msgRelflectType, "*")

	msg, exist := lmsg.messages.Get(msgRelflectType)

	if !exist {
		return false
	}

	msgReflect.Elem().Set(reflect.ValueOf(msg))
	return true
}
