package message

import (
	"reflect"
	"strings"
)

func (lmsg *mapBased) Extract(msgtype any) (exist bool) {

	msgReflect := reflect.ValueOf(msgtype)
	msgRelflectType := msgReflect.Type().String()
	msgRelflectType = strings.Trim(msgRelflectType, "*")

	msg, exist := lmsg.messages.Get(msgRelflectType)

	if !exist {
		return false
	}

	msgReflect.Elem().Set(reflect.ValueOf(msg))

	lmsg.messages.Delete(msgRelflectType)

	return true
}
