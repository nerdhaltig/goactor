package message

import "gitlab.com/nerdhaltig/goactor/v2/safemap"

type PackedMessage interface {

	// Pack add a message to the pack
	Pack(message any)

	// UnPack return the first message in the pack and remove it.
	// It return nil if no message exist anymore.
	UnPack() (message any)

	// As load the type of msgtype from the pack and write it to msgtype.
	// This return false if the type of msgtype not exist.
	// Only one message per type can exist in the pack !
	//
	// Example:
	//
	//  var message *customMessage
	//  if ctx.As(&message) {
	As(msgtype any) (exist bool)

	// Extract do the same as As() but removes the message from the pack
	Extract(msgtype any) (exist bool)
}

type mapBased struct {
	messages *safemap.SafeMap[string, any]
}

func NewMapBasedPackedMessage() PackedMessage {
	newPackedMessage := &mapBased{
		messages: safemap.New[string, any](),
	}

	return newPackedMessage
}
