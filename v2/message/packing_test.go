package message_test

import (
	"testing"

	"github.com/alecthomas/assert/v2"
	"gitlab.com/nerdhaltig/goactor/v2/message"
)

func TestPacking(t *testing.T) {

	type Message1 struct{ Value string }
	type Message2 struct{ Value string }
	type Message3 struct{ Value string }
	type NotPacked struct{}

	pMessage := message.NewMapBasedPackedMessage()
	pMessage.Pack(nil)
	pMessage.Pack(&Message1{Value: "1"})
	pMessage.Pack(&Message2{Value: "2"})
	pMessage.Pack(&Message3{Value: "3"})
	pMessage.Pack("a string")
	pMessage.Pack(42)
	pMessage.Pack(42.42)

	// unpack some messages
	message1 := &Message1{}
	pMessage.As(&message1)
	assert.NotZero(t, message1)

	message3 := &Message3{}
	pMessage.As(&message3)
	assert.NotZero(t, message3)

	message2 := &Message2{}
	pMessage.As(&message2)
	assert.NotZero(t, message2)

	// this should not work
	notPacked := &NotPacked{}
	assert.False(t, pMessage.As(&notPacked))

	// get the static string
	staticString := ""
	assert.True(t, pMessage.As(&staticString))
	assert.Equal(t, "a string", staticString)

	// Extract ( and remove ) a message
	messageToExtract := &Message2{}
	assert.True(t, pMessage.Extract(&messageToExtract))
	assert.NotZero(t, messageToExtract)
	assert.Equal(t, "2", message2.Value)

	assert.False(t, pMessage.Extract(&notPacked))
	assert.False(t, pMessage.As(&messageToExtract))

	// unpack
	upMessage := pMessage.UnPack()
	assert.NotZero(t, upMessage)

	upMessage = pMessage.UnPack()
	assert.NotZero(t, upMessage)

	upMessage = pMessage.UnPack()
	assert.NotZero(t, upMessage)

	upMessage = pMessage.UnPack()
	assert.NotZero(t, upMessage)

	upMessage = pMessage.UnPack()
	assert.NotZero(t, upMessage)

	upMessage = pMessage.UnPack()
	assert.Zero(t, upMessage)
}
