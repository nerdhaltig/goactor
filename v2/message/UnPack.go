package message

func (lmsg *mapBased) UnPack() (message any) {
	return lmsg.messages.DeleteFirst()
}
