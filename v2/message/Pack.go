package message

import (
	"reflect"
	"strings"
)

func (msg *mapBased) Pack(message any) {
	if message == nil {
		return
	}

	msgType := reflect.TypeOf(message).String()
	msgType = strings.Trim(msgType, "*")

	msg.messages.Set(msgType, message)
}
