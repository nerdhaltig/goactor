package watcherbased

import (
	"reflect"

	"gitlab.com/nerdhaltig/goactor/v2/async/ports"
)

func (ctx *adapterData) Provide(valueType any) (currentCtx ports.AsyncContext) {
	if valueType != nil {

		// value-type
		valueReflect := reflect.ValueOf(valueType)
		valueType := valueReflect.Type().String()

		// provide the value
		_, cVarExist := ctx.elements.Get(valueType)
		if !cVarExist {
			cVar := &contextVariable{
				watcherMap: make(map[chan any]bool),
			}
			ctx.elements.Set(valueType, cVar)
		}
	}

	return ctx
}
