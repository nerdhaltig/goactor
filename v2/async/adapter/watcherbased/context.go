package watcherbased

import (
	"time"
)

// Deadline implements ports.AsyncContext.
func (ctx *adapterData) Deadline() (deadline time.Time, ok bool) {
	return ctx.ctxParent.Deadline()
}

// Done implements ports.AsyncContext.
func (ctx *adapterData) Done() <-chan struct{} {
	return ctx.ctxParent.Done()
}

// Err implements ports.AsyncContext.
func (ctx *adapterData) Err() error {
	if ctx.err != nil {
		return ctx.err
	}
	return ctx.ctxParent.Err()
}
