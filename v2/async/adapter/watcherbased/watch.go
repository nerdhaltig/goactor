package watcherbased

import (
	"reflect"

	"gitlab.com/nerdhaltig/goactor/v2/async/domain"
)

// Watch implements domain.AsyncContext.
func (ctx *adapterData) Watch(valueType any, watcherFunc func(newValue any) error) (err error) {

	msgReflect := reflect.ValueOf(valueType)
	msgRelflectType := msgReflect.Type().String()

	// get context value if there is one
	cVal, cValExist := ctx.elements.Get(msgRelflectType)
	if !cValExist {
		return domain.ErrNotProvided
	}

	// register watcher
	watcher := make(chan any, 1)
	cVal.lock.Lock()
	cVal.watcherMap[watcher] = true
	cVal.lock.Unlock()

	// deregister on return
	defer func() {
		cVal.lock.Lock()
		delete(cVal.watcherMap, watcher)
		close(watcher)
		cVal.lock.Unlock()
	}()

loop1:
	for {
		select {
		case <-ctx.ctxParent.Done():
			err = ctx.ctxParent.Err()
			break loop1
		case newValue, ok := <-watcher:
			if ok {
				watcherErr := watcherFunc(newValue)
				if watcherErr != nil {
					err = watcherErr
					break loop1
				}
			} else {
				err = domain.ErrContextCancelled
			}

		}

	}

	return
}
