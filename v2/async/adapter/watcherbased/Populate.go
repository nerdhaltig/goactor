package watcherbased

func (ctx *adapterData) Populate(providedValueType any, populateFunc func() any) {

	// inform the context that we provide a new value
	ctx.Provide(providedValueType)

	go func() {
		// call and get new value
		newValue := populateFunc()
		ctx.Set(newValue)
	}()

}
