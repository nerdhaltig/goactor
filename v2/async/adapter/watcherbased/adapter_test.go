package watcherbased_test

import (
	"context"
	"sync"
	"testing"
	"time"

	"github.com/rs/zerolog/log"
	"gitlab.com/nerdhaltig/goactor/v2/async"
	"gitlab.com/nerdhaltig/goactor/v2/async/adapter/watcherbased"
	"gitlab.com/nerdhaltig/goactor/v2/async/ports"
	"go.uber.org/goleak"
)

func TestProvide(t *testing.T) {
	defer goleak.VerifyNone(t)

	ctxTimeout, ctxCancel := context.WithCancel(context.Background())
	defer ctxCancel()

	ctx := watcherbased.NewContext(watcherbased.NewContextOpts{
		ParentCtx: ctxTimeout,
	})
	async.TestProvide(t, ctx)
}

func TestReturnOfValue(t *testing.T) {
	defer goleak.VerifyNone(t)

	ctxTimeout, ctxCancel := context.WithTimeout(context.Background(), time.Second*3)
	defer ctxCancel()

	ctx := watcherbased.NewContext(watcherbased.NewContextOpts{
		ParentCtx: ctxTimeout,
	})
	async.TestReturnOfValue(t, ctx)
}

func TestValue(t *testing.T) {
	defer goleak.VerifyNone(t)

	ctx := watcherbased.NewContext()
	async.TestValue(t, ctx)
}

func TestTimeouts(t *testing.T) {
	defer goleak.VerifyNone(t)

	ctxTimeout, ctxCancel := context.WithTimeout(context.Background(), time.Second*1)
	ctx := watcherbased.NewContext(watcherbased.NewContextOpts{
		ParentCtx: ctxTimeout,
	})
	defer ctxCancel()
	async.TestTimeout(t, ctx)
}

func TestCancelled(t *testing.T) {
	defer goleak.VerifyNone(t)

	ctxTimeout, ctxCancel := context.WithTimeout(context.Background(), time.Second*5)
	ctx := watcherbased.NewContext(watcherbased.NewContextOpts{
		ParentCtx: ctxTimeout,
	})
	defer ctxCancel()
	async.TestCancelled(t, ctxCancel, ctx)
}

func TestMessaging(t *testing.T) {
	defer goleak.VerifyNone(t)

	ctxTimeout, ctxCancel := context.WithCancel(context.Background())
	defer ctxCancel()

	ctx := watcherbased.NewContext(watcherbased.NewContextOpts{ParentCtx: ctxTimeout})
	Messaging(t, ctx)
}

type testBroadcastMessage struct{ Message string }

func Messaging(t *testing.T, ctx ports.AsyncContext) {

	// provide for messaging
	ctx.Provide(&testBroadcastMessage{})

	wg := sync.WaitGroup{}
	wg.Wait()

	// worker
	workerMax := 2
	for workerIndex := 0; workerIndex < workerMax; workerIndex++ {
		currentWorkerIndex := workerIndex
		go func() {
			ctx.Watch(&testBroadcastMessage{}, func(newValue any) error {
				if msg, ok := newValue.(*testBroadcastMessage); ok {
					log.Debug().Int("worker", currentWorkerIndex).Msg(msg.Message)
					time.Sleep(time.Millisecond * 500)
					wg.Done()
				}
				return nil
			})
		}()
	}

	time.Sleep(time.Millisecond * 200)

	wg.Add(workerMax)
	ctx.Set(&testBroadcastMessage{Message: "first"})
	//wg.Wait()

	wg.Add(workerMax)
	ctx.Set(&testBroadcastMessage{Message: "second"})
	wg.Wait()
}
