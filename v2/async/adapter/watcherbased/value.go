package watcherbased

import (
	"reflect"
	"strings"

	"gitlab.com/nerdhaltig/goactor/v2/async/domain"
)

func (ctx *adapterData) Value(key any) (value any) {

	msgReflect := reflect.ValueOf(key)
	msgRelflectType := msgReflect.Type().String()
	msgRelflectType = strings.TrimPrefix(msgRelflectType, "*")

	// if key is a string, we use the string
	if msgRelflectType == "string" {
		msgRelflectType = key.(string)
	}

	// get context value if there is one
	cVal, cValExist := ctx.elements.Get(msgRelflectType)
	if !cValExist {
		ctx.err = domain.ErrNotExist
		return
	}

	// no value there, we wait
	if cVal.value == nil {

		// register watcher
		watcher := make(chan any, 1)
		cVal.lock.Lock()
		// its not an permanent-watcher
		// so the channel will be closed by Set()
		cVal.watcherMap[watcher] = false
		cVal.lock.Unlock()

		// deregister on return
		defer func() {
			cVal.lock.Lock()
			delete(cVal.watcherMap, watcher)
			cVal.lock.Unlock()
		}()

		// wait
		select {
		case <-ctx.Done():
			ctx.err = ctx.Err()

			return
		case newValue, ok := <-watcher:
			if ok {
				value = newValue
				return
			} else {
				ctx.err = domain.ErrWatcherClosed
			}
		}

	} else {
		value = cVal.value
	}

	return
}
