// Package watcherbased provide an adapter based in watcher-pattern
//
// This Adapter use channels to inform As() and Watch() which is more deterministic
package watcherbased

import (
	"context"
	"sync"

	"gitlab.com/nerdhaltig/goactor/v2/async/ports"
	"gitlab.com/nerdhaltig/goactor/v2/safemap"
)

type adapterData struct {
	ctxParent context.Context
	ctxCancel context.CancelFunc
	elements  *safemap.SafeMap[string, *contextVariable]
	err       error
}

type contextVariable struct {
	lock       sync.Mutex
	value      any
	watcherMap map[chan any]bool
}

type NewContextOpts struct {
	ParentCtx context.Context
}

// NewContext will create a new async-context
func NewContext(opts ...NewContextOpts) ports.AsyncContext {

	var opt NewContextOpts
	if len(opts) > 0 {
		opt = opts[len(opts)-1]
	}

	newAdapter := &adapterData{
		ctxParent: opt.ParentCtx,
		elements:  safemap.New[string, *contextVariable](),
	}

	if newAdapter.ctxParent == nil {
		newAdapter.ctxParent, newAdapter.ctxCancel = context.WithCancel(context.Background())
	} else {
		newAdapter.ctxParent, newAdapter.ctxCancel = context.WithCancel(opt.ParentCtx)
	}

	return newAdapter
}
