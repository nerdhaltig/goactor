package watcherbased

import (
	"reflect"
)

// As will wait until somebody provide the message in the context
// only on timeout an err != nil
//
// Example:
//
// var message *customMessage
// ctx.As(&message)
func (ctx *adapterData) As(value any) (exist bool, err error) {

	msgReflect := reflect.ValueOf(value)

	curValue := ctx.Value(value)
	err = ctx.Err()

	// get data and set the value
	if curValue != nil && err == nil {
		exist = true
		msgReflect.Elem().Set(reflect.ValueOf(curValue))
	}

	return
}
