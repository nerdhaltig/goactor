package watcherbased

import (
	"reflect"

	"gitlab.com/nerdhaltig/goactor/v2/async/ports"
)

func (ctx *adapterData) Set(value any) (currentCtx ports.AsyncContext) {
	if value != nil {
		// value-type
		valueReflect := reflect.ValueOf(value)
		valueType := valueReflect.Type().String()

		// get context-Variable or create it
		if cVar, cVarExist := ctx.elements.Get(valueType); cVarExist {

			// lock
			cVar.lock.Lock()
			defer cVar.lock.Unlock()

			// set value
			cVar.value = value

			// inform watcher
			for watcher, keepChannel := range cVar.watcherMap {
				watcher <- value
				if !keepChannel {
					close(watcher)
				}
			}

		}
	}

	return ctx
}
