//go:build !production
// +build !production

package async

import (
	"context"
	"testing"
	"time"

	"github.com/alecthomas/assert"
	"gitlab.com/nerdhaltig/goactor/v2/async/ports"
)

type testProvideValueType struct{ Test string }
type firstValueType string
type secondValueType string
type thirdValueType string

func TestTimeouts(t *testing.T, ctx ports.AsyncContext) {
	TestTimeout(t, ctx)
	TestTimeoutOnExistingValue(t, ctx)
}

func TestProvide(t *testing.T, ctx ports.AsyncContext) {

	// we provide some values ( but not set them yet ! )
	ctx.
		Provide(&testProvideValueType{}).
		Provide("a string").
		Provide(int8(42))

	var returnedValueExist bool
	var returnedValueErr error

	// we set our struct
	ctx.Set(&testProvideValueType{Test: "first"})
	ctx.Set(&testProvideValueType{Test: "second"})
	ctx.Set(&testProvideValueType{Test: "dummy"})

	// We store a pointer ! so we want a pointer back !
	var providedValue *testProvideValueType
	returnedValueExist, returnedValueErr = ctx.As(&providedValue)
	assert.True(t, returnedValueExist)
	assert.NoError(t, returnedValueErr)
	assert.Equal(t, "dummy", providedValue.Test)

	// we set an int
	ctx.Set(int8(42))

	var intValue int8
	returnedValueExist, returnedValueErr = ctx.As(&intValue)
	assert.True(t, returnedValueExist)
	assert.NoError(t, returnedValueErr)
	assert.Equal(t, int8(42), intValue)
}

func TestReturnOfValue(t *testing.T, ctx ports.AsyncContext) {

	ctx.Populate(firstValueType(""), func() any {
		time.Sleep(time.Second * 2)
		return firstValueType("dummy")
	})

	var returnedValue firstValueType
	returnedValueExist, returnedValueErr := ctx.As(&returnedValue)
	assert.True(t, returnedValueExist)
	assert.NoError(t, returnedValueErr)
	assert.Equal(t, "dummy", string(returnedValue))
}

func TestValue(t *testing.T, ctx ports.AsyncContext) {

	// we provide a pointer to an struct instance
	ctx.Provide(&testProvideValueType{}).
		Set(&testProvideValueType{Test: "an value"})

	// we try to get the value by name of our struct
	returnValueAny := ctx.Value("*async.testProvideValueType")
	returnValue, ok := returnValueAny.(*testProvideValueType)
	assert.True(t, ok)
	assert.Equal(t, "an value", returnValue.Test)

	// cool, now we try the same with an pointer to the same type
	returnPointer := &testProvideValueType{}
	returnValueAny = ctx.Value(&returnPointer)
	returnValue, ok = returnValueAny.(*testProvideValueType)
	assert.True(t, ok)
	assert.Equal(t, "an value", returnValue.Test)

}

func TestTimeout(t *testing.T, ctx ports.AsyncContext) {

	ctx.Populate(secondValueType(""), func() any {
		timer := time.NewTimer(time.Second * 3)
		select {
		case <-timer.C:
			return secondValueType("second")
		case <-ctx.Done():
			return nil
		}
	})

	var returnedValue secondValueType
	returnedValueExist, returnedValueErr := ctx.As(&returnedValue)
	assert.False(t, returnedValueExist)
	assert.Equal(t, context.DeadlineExceeded, returnedValueErr)
}

func TestTimeoutOnExistingValue(t *testing.T, ctx ports.AsyncContext) {

	ctx.Populate(thirdValueType(""), func() any {
		time.Sleep(time.Second * 3)
		return thirdValueType("third")
	})

	var returnedValue thirdValueType
	returnedValueExist, returnedValueErr := ctx.As(&returnedValue)
	assert.True(t, returnedValueExist)
	assert.NoError(t, returnedValueErr)
	assert.Contains(t, "third", string(returnedValue))

	returnedValueExist, returnedValueErr = ctx.As(&returnedValue)
	assert.True(t, returnedValueExist)
	assert.NoError(t, returnedValueErr)
	assert.Equal(t, "third", string(returnedValue))

}

type testCancelledValueType string

func TestCancelled(t *testing.T, ctxCancelFct context.CancelFunc, ctx ports.AsyncContext) {

	// we want this, so we just inform our context
	ctx.Provide(testCancelledValueType(""))

	// we cancel the context in the background
	go func() {
		time.Sleep(time.Second * 1)
		ctxCancelFct()
	}()

	// we wait
	var returnedValue testCancelledValueType
	_, err := ctx.As(&returnedValue)
	assert.Equal(t, context.Canceled, err)
}
