// Package ports provide ports for async-context
package ports

import (
	"context"
	"time"
)

// AsOptions represents additional options for As()
type AsOptions struct {
	Timeout time.Duration // Timeout on wait for variable
}

// AsyncContext an async context for simple actors
type AsyncContext interface {
	context.Context

	// Provide says that we provide an value in this context,
	// which will be set later with Set().
	//
	// valueType can be anything but should be an empty value, you
	// don't need to fill it.
	//
	// For example: Provide(&myObject{})
	//
	// this function just return the same context for chaining
	Provide(valueType any) (currentCtx AsyncContext)

	// Set will set an value that was "provided" in the context
	// and will inform all that are waiting for this var
	//
	// You need to provide a variable before calling this
	// if not, nothing will be done inside this function
	//
	// this function just return the same context for chaining
	Set(value any) (currentCtx AsyncContext)

	// Populate a message to the context via an function
	//
	// this basically combines Provide() with Set() via an function
	// that runs as go-routine in the background
	Populate(providedValueType any, populateFunc func() any)

	// As will wait until somebody Set() the variable in this context
	//
	// If the variable already is set, we not wait, you just get the variable
	//
	// If your parent context get cancelled you get the error of the parent context
	//
	// Example:
	//
	// var message *customMessage
	// ctx.As(&message)
	As(value any) (exist bool, err error)

	// Watch for an value only on change
	//
	// if you watcherFunc returns an err, we stop watching
	Watch(valueType any, watcherFunc func(newValue any) error) (err error)
}
