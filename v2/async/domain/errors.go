package domain

import "errors"

var ErrNotProvided = errors.New("value is not provided")
var ErrTimeout = errors.New("timeout")
var ErrContextCancelled = errors.New("context cancelled")
var ErrWatcherClosed = errors.New("watcher channel closed")
var ErrNotExist = errors.New("value not exist in context")
